testnet = True
production = True
autoload = False
version = 1
subscribed = ['b6a95f94fef093ee9009b04a09ecb9cb5cba20ab6f13fe0926aeb27b8671df43']

''' 
testnet:
    Boolean dictating whether the network is mainnet or testnet

production:
    Boolean dictating whether to query production assets or non-production assets.

autoload:
    Boolean which, when True, loads the P2TH key corresponding to the current network
    and production state into the local node. If walletnotify is going to be used it is
    recommended that autoload is set to False. If False then papi is informed of only new 
    transactions pertaining to subscribed decks.

version:
    Integer corresponding to the PeerAssets protocol version being used.

subscribed:
    List of deck id's that will be taken as P2TH keys, imported into the local node, and
    be processed into the local database. This process includes recording one entry into 
    the Deck table and continual entries into the Card table.          

'''
